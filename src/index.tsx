import React from 'react';
import { createRoot } from 'react-dom/client';
import './app/styles/index.scss';
import App from './app/App';
import { WagmiProvider } from './app/providers/WagmiProvider';

const root = createRoot(document.getElementById('root'));

root.render(
    <WagmiProvider>
        <App />
    </WagmiProvider>,
);

import React, { FunctionComponent } from 'react';
import { classNames } from 'shared/helpers/lib';
import Logo from 'shared/assets/icons/logo.svg';
import Fb from 'shared/assets/icons/facebook.svg';
import Twt from 'shared/assets/icons/twtiiter.svg';
import Yt from 'shared/assets/icons/youtube.svg';
import Inst from 'shared/assets/icons/instagramm.svg';
import cls from './Footer.module.scss';

interface FooterProps {
    className?: string,
}

export const Footer: FunctionComponent = ({ className }: FooterProps) => (
    <div className={classNames(cls.Footer, {}, [className])}>
        <div className={classNames(cls.info)}>
            <div>
                Privacy Policy
            </div>
            <div>
                Terms & Conditions
            </div>
            <div>
                Cookie Policy
            </div>
        </div>
        <div className={classNames(cls.logos)}>
            <Logo />
            <div>
                ©2022 All rights reserved. Powered by Atla
            </div>
        </div>
        <div className={classNames(cls.links)}>
            <div className={classNames(cls.links_wrapper)}>
                <div className={classNames(cls.link)}>
                    <a href="https://facebook.com" target="_blank" rel="noreferrer">
                        <Fb />
                    </a>
                </div>
                <div className={classNames(cls.link)}>
                    <a href="https://x.com" target="_blank" rel="noreferrer">
                        <Twt />
                    </a>
                </div>
                <div className={classNames(cls.link)}>
                    <a href="https://youtube.com" target="_blank" rel="noreferrer">
                        <Yt />
                    </a>
                </div>
                <div className={classNames(cls.link)}>
                    <a href="https://instagram.com" target="_blank" rel="noreferrer">
                        <Inst />
                    </a>
                </div>
            </div>
        </div>
    </div>
);

import { classNames } from 'shared/helpers/lib';
import React, { useCallback, useState } from 'react';
import { useAccount, useDisconnect } from 'wagmi';
import { Button, ThemeButtonEnum } from 'shared/ui/Button';
import { Modal } from 'shared/ui/Modal';
import Logo from 'shared/assets/icons/logo.svg';
import { getShortAddress } from 'shared/helpers';
import WalletIcon from 'shared/assets/icons/metmask.svg';
import WalletOptions from 'shared/assets/icons/walletoptions.svg';
import cls from './Navbar.module.scss';
import { WalletConnections } from '../../WalletConections';
import { Chains } from '../../Chains';

interface NavbarProps {
    className?: string;
}

export const Navbar = ({ className }: NavbarProps) => {
    const [isOpenConnect, setIsOpenConnect] = useState(false);
    const [isOpenOptions, setIsOpenOptions] = useState(false);
    const { isConnected, address } = useAccount();
    const { disconnect } = useDisconnect();

    const showModalConnect = useCallback(() => {
        setIsOpenConnect((prev) => !prev);
    }, []);

    const showAccountOptions = useCallback(() => {
        setIsOpenOptions((prev) => !prev);
    }, []);

    const disconnectHandler = useCallback(() => {
        disconnect();
        setIsOpenOptions((prev) => !prev);
    }, []);

    return (
        <div className={classNames(cls.Navbar, {}, [className])}>
            <div className={classNames(cls.logos, {}, [])}>
                <Logo />
            </div>
            <div className={classNames(cls.wallets, {}, [])}>
                {isConnected
                    ? (
                        <>
                            <Chains />
                            <div className={classNames(cls.account)}>
                                <Button
                                    theme={ThemeButtonEnum.CONNECTED}
                                    onClick={showAccountOptions}
                                >
                                    <div className={classNames(cls.buttonInner)}>
                                        <WalletIcon />
                                        {getShortAddress(address, 10)}
                                        <WalletOptions />
                                    </div>
                                </Button>
                                {isOpenOptions && (
                                    <Button
                                        theme={ThemeButtonEnum.CONNECTED}
                                        onClick={disconnectHandler}
                                        className={classNames(cls.disconnect)}
                                    >
                                        Disconnect
                                    </Button>
                                )}
                            </div>
                        </>
                    )
                    : (
                        <Button
                            theme={ThemeButtonEnum.CONNECT}
                            onClick={showModalConnect}
                        >
                            Connect Wallet
                        </Button>
                    )}
            </div>
            <Modal isOpen={isOpenConnect} onClose={showModalConnect}>
                <WalletConnections onClose={showModalConnect} />
            </Modal>
        </div>
    );
};

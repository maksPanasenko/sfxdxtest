import React, { FunctionComponent, useCallback } from 'react';
import { classNames } from 'shared/helpers/lib';
import { Button, ThemeButtonEnum } from 'shared/ui/Button';
import { useNetwork, useSwitchNetwork } from 'wagmi';
import cls from './SwitchChains.module.scss';

interface SwitchChainsProps {
    onClose: () => void;
    className?: string;
}

export const SwitchChains: FunctionComponent<SwitchChainsProps> = ({ onClose, className }) => {
    const { chain: currentChain } = useNetwork();
    const {
        chains, isLoading, pendingChainId, switchNetwork,
    } = useSwitchNetwork();

    const switchChain = useCallback((id) => {
        switchNetwork?.(id);
        onClose();
    }, [onClose]);
    return (
        <div className={classNames(cls.SwitchChains, {}, [className])}>
            <div className={classNames(cls.chainsInner)}>
                {chains.map((chain) => (
                    <Button
                        disabled={!switchNetwork || chain.id === currentChain?.id}
                        key={chain.id}
                        onClick={() => switchChain(chain.id)}
                        theme={ThemeButtonEnum.CONNECTED}
                        className={classNames(cls.chains)}
                    >
                        {chain.name}
                        {isLoading && pendingChainId === chain.id && ' (switching)'}
                    </Button>
                ))}
            </div>
        </div>
    );
};

import React, { FunctionComponent, useEffect, useState } from 'react';
import { classNames } from 'shared/helpers/lib';
import { useAccount, useNetwork } from 'wagmi';
import {
    createPublicClient, http, formatEther,
} from 'viem';
import { goerli } from 'viem/chains';
import 'viem/window';
import cls from './Content.module.scss';

interface ContentProps {
    className?: string,
}

export const Content: FunctionComponent = ({ className }: ContentProps) => {
    const { address, isConnected } = useAccount();
    const { chain } = useNetwork();
    const [balance, setBalance] = useState<string | null>(null);

    const pubLicClient = createPublicClient({
        chain: goerli,
        transport: http('https://rpc.goerli.eth.gateway.fm'),
    });

    useEffect(() => {
        if (isConnected) {
            const getTokenBalance = async () => {
                const tokenBalance = await pubLicClient.getBalance({
                    address,
                });
                setBalance(formatEther(tokenBalance));
            };
            getTokenBalance();
        }
    }, [isConnected]);

    return (
        <div className={classNames(cls.Content, {}, [className])}>
            {isConnected
                ? chain.id === 5
                    ? (
                        <div className={classNames(cls.pageContent)}>
                            {`Your Adress is ${address}`}
                            <br />
                            {`Balance is ${balance} ETH`}
                        </div>
                    )
                    : <div> Please Switch to Goerli </div>
                : <div>Please connect wallet to see you balance</div>}
        </div>
    );
};

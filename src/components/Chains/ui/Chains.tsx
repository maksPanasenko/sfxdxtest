import React, { FunctionComponent, useCallback, useState } from 'react';
import { classNames } from 'shared/helpers/lib';
import { useNetwork } from 'wagmi';
import cls from './Chains.module.scss';
import { SwitchChains } from '../../SwitchChains';

interface ChainsProps {
    className?: string;
}

export const Chains: FunctionComponent = ({ className }: ChainsProps) => {
    const [showOtherChains, setShowOtherChains] = useState(false);
    const { chain } = useNetwork();

    const showSwitchChain = useCallback(() => {
        setShowOtherChains((prev) => !prev);
    }, []);

    return (
        <div className={classNames(cls.Chains, {}, [className])}>
            {chain && (
                <div onClick={showSwitchChain}>
                    {`Connected to ${chain.name}`}
                </div>
            )}
            {showOtherChains && <SwitchChains onClose={showSwitchChain} />}
        </div>
    );
};

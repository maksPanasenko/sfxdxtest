import { FunctionComponent, ReactNode } from 'react';
import { configureChains, createConfig, WagmiConfig } from 'wagmi';
import { bsc, goerli, mainnet } from 'wagmi/chains';
import { jsonRpcProvider } from 'wagmi/providers/jsonRpc';
import { publicProvider } from 'wagmi/providers/public';
import { MetaMaskConnector } from 'wagmi/connectors/metaMask';
import { WalletConnectConnector } from 'wagmi/connectors/walletConnect';

interface WagmiProviderProps {
    children: ReactNode
}

const { chains, publicClient, webSocketPublicClient } = configureChains(
    [goerli, bsc, mainnet],
    [
        jsonRpcProvider({
            rpc: () => ({
                http: 'https://rpc.goerli.eth.gateway.fm',
            }),
        }),
        publicProvider(),
    ],
);

const config = createConfig({
    autoConnect: true,
    publicClient,
    webSocketPublicClient,
    connectors: [
        new MetaMaskConnector({
            chains,
            options: {
                shimDisconnect: true,
                UNSTABLE_shimOnConnectSelectAccount: true,
            },
        }),
        new WalletConnectConnector({
            chains,
            options: {
                projectId: '8be093c7ce0f2468dcada37d44270c05',
                showQrModal: true,
            },
        }),
    ],
});

export const WagmiProvider: FunctionComponent<WagmiProviderProps> = ({ children }) => (
    <WagmiConfig config={config}>
        {children}
    </WagmiConfig>

);

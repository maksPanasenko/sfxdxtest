import React, { FC } from 'react';
import { classNames } from 'shared/helpers/lib';
import { Divider } from 'shared/ui/Divider';
import { Navbar } from 'components/NavBar';
import { Content } from 'components/Content/';
import { Footer } from 'components/Footer';

const App: FC = () => (
    <div className={classNames('app', {}, [])}>
        <Navbar />
        <Divider />
        <div className="content_wrapper">
            <Content />
        </div>
        <Divider />
        <Footer />
    </div>
);

export default App;

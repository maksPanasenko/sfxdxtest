export const getShortAddress = (address: null | string, startOffset?: number) => {
    if (!address) {
        return '';
    }

    if (startOffset && address.length <= startOffset) {
        return address;
    }

    const offset = address.length - (startOffset || 0) <= 3 ? address.length - 6 : startOffset;

    return `${address.slice(0, offset || 2)}...${address.slice(-3)}`;
};

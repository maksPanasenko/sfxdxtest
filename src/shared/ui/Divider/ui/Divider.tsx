import React, { FunctionComponent } from 'react';
import { classNames } from 'shared/helpers/lib';
import cls from './Divider.module.scss';

interface DividerProps {
    className?: string,
}

export const Divider: FunctionComponent = ({ className }: DividerProps) => (
    <div className={classNames(cls.Divider, {}, [className])} />
);
